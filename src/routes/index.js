import smsBrandnameController from "~/controllers/sms-brandname";
import error from './middlewares/error';

require('express-group-routes');

export default (router, deps) => {

  router.group('/v1', (r) => {
    r.get('/send-sms-otp', smsBrandnameController.get(deps));
  })

  router.use(error);

  return router;
}