export const successResponse = (res, data = {}, meta) => {
  res.status(200).send({ data, meta });
};

export const validateResponse = (res, message = 'Not Found', data = []) => {
  res.status(422).send({ message, data });
};

export const badRequest = (res, data) => {
  res.status(400).send({
    message: 'Bad Request',
    data,
  });
};

export const serverError = (res, error) => {
  res.status(500).send({
    message: 'Server Errors.',
    data: error,
  });
};