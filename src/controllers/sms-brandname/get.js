import { successResponse, validateResponse } from '~/routes/response';
import SmsBrandNameService from '~/services/sms-brandname-service';
import { isEmpty, isString, isError } from 'lodash'
import { htmlCompile } from '~/routes/utils';

/**
 * @param {Object} deps
 * @param {Models} deps.models
 * @param {SmsBrandNameService} deps.smsBrandNameService
 */

export default ({ smsBrandNameService }) => [
  async (req, res) => {
    const { phone } = req.query;
    if(isEmpty(phone) || !isString(phone)) {
      return validateResponse(res, "Phone is not validate");
    }

    const message = htmlCompile("sms-otp", {});
    const results = await smsBrandNameService.sentOTP({phone, message}).catch(err => err);

    if(isError(results)) { // TODO them dieu kien loi
      return badRequest(res, "Sending message failed");
    }
    return successResponse(res)
  }
]