import express from 'express';
import cors from 'cors';
import hbs from 'express-hbs';
import * as config from '~/config';
import routes from '~/routes';
import SmsBrandNameService from './services/sms-brandname-service';

require('express-async-errors');

const app = express();
app.use(cors());
app.use(
  express.urlencoded({
    extended: true,
    limit: '500mb'
  }),
);
app.use(express.json({limit: '500mb'}));
app.set('trust proxy', true)
app.engine(
  'hbs',
  hbs.express4({
    partialsDir: `${__dirname}/handlers/template/partials`,
  }),
);
app.set('view engine', 'hbs');
app.set('views', `${__dirname}/handlers/template/view`);

const smsBrandNameService = new SmsBrandNameService({config: config.SMS_BRANDNAME});

const deps = {
  config,
  smsBrandNameService
}


const server = routes(app, deps).listen(config.PORT, '0.0.0.0',  () => {
  console.log(`Listening at port: ${config.PORT}`);
});

const shutdown = () => {
  console.info('SIGTERM signal received.');
  console.log('Closing http server.');

  return server.close(async () => {
    console.log('Http server closed.');
    process.exit(0);
  });
};

module.exports = { server, shutdown };

process.on('SIGTERM', shutdown);
