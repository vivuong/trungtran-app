import dotenv from 'dotenv';

if (process.env.ENVIRONMENT === 'dev') {
  dotenv.config();
}

export const SMS_BRANDNAME = {
  CLIENT_ID: process.env.SMS_BRANDNAME_CLIENT_ID,
  CLIENT_SECRET: process.env.SMS_BRANDNAME_CLIENT_SECRET,
}

export const {
  PORT
} = process.env;