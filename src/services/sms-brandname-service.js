import moment from 'moment';
import { axios } from '~/routes/utils';

export default class SmsBrandNameService {
  constructor({ config }) {
    this.config = config;
  }

  getTokenSMS() { // TODO
    const {CLIENT_ID, CLIENT_SECRET} = this.config;

    return axios({
      method: 'post',
      url: `/oauth2/token`,
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        scope: 'send_brandname_otp',
        session_id: moment().format('x'),
        grant_type: 'client_credentials',
      }
    })
  };

  async sentOTP({ phone, message }) { // TODO
    const accessToken = await this.getTokenSMS();
    return axios({
      method: 'post',
      url: `/api/push-brandname-otp`,
      headers: {
        "Content-Type": "application/json",
      },
      data: {
        access_token: accessToken,
        session_id: moment().format('x'),
        BrandName: 'Maico',
        Phone: phone,
        Message: message,
        RequestId: `tranID-Core01-${moment().format('x')}`,
      }
    });
  };

}