# README
This is the API Backend for the ...

## Some useful command?

```yml
# To Run the APIs server http://127.0.0.1:3000
npm run start

# To build the project for Production
npm run build

# To run project on Production
npm run serve

# To build docker image
docker build -t apis_app .

# To run docker compose
docker-compose up -d

```