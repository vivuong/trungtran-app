import * as rax from 'retry-axios';
import httpClient from 'axios';

const fs = require('fs');
const Handlebars = require('handlebars');
const path = require('path');
const axiosClient = httpClient.create();
axiosClient.defaults.timeout = 4000;
axiosClient.defaults.raxConfig = {
    instance: axiosClient,
};
const interceptorId = rax.attach(axiosClient);

export const axios = (config) => {
    return axiosClient({
        ...config,
        raxConfig: {
            noResponseRetries: 3,
            retryDelay: 2000,
            httpMethodsToRetry: ['GET', 'HEAD', 'OPTIONS', 'DELETE', 'PUT', 'POST'],
            instance: axiosClient,
            onRetryAttempt: err => {
                const cfg = rax.getConfig(err);
                console.log(`Retry attempt #${cfg.currentRetryAttempt}`);
            }
        }
    })
}

export const htmlCompile = (hbsName, data) => {
    const source = fs.readFileSync(path.join(__dirname, `../handlers/template/view/${hbsName}.hbs`), 'utf-8');
    return Handlebars.compile(source)(data);
}